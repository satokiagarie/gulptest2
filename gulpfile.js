const { src, dest, parallel, series, watch } = require('gulp');
const pug = require('gulp-pug');
const indent = require('gulp-html-beautify');
const rename = require('gulp-rename');
const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const mmq = require('gulp-merge-media-queries');
const ts = require('gulp-typescript');
// const minifyCSS = require('gulp-csso');
// const concat = require('gulp-concat');

function pugHtmlTask() {
    return src(['src/pug/html/**/*.pug', '!src/pug/html/**/_*.pug'])
    .pipe(pug({
        pretty: true
    }))
    .pipe(indent({
        indent_size: 4,
        // indent_with_tabs: true
    }))
    .pipe(dest('dist/html'))
}
function pugWpTask() {
    return src(['src/pug/wp/**/*.pug', '!src/pug/wp/**/_*.pug'])
    .pipe(pug({
        pretty: true
    }))
    .pipe(indent({
        indent_size: 4,
        // indent_with_tabs: true
    }))
    .pipe(rename(function(path){
        path.extname = ".php";
    }))
    .pipe(dest('dist/wp'))
}

function scssTask() {
    return src('src/scss/**/*.scss')
    .pipe(sass({
        outputStyle: "expanded",
        // sourcemaps: true
    }))
    .pipe( postcss([ autoprefixer({
        // ☆IEは11以上、Androidは4.4以上
        // その他は最新2バージョンで必要なベンダープレフィックスを付与する
        "overrideBrowserslist": ["last 2 versions", "ie >= 11", "Android >= 4"],
        cascade: false
    }) ]) )
    // .pipe(minifyCSS())
    .pipe(mmq())
    .pipe(dest('dist/html/assets/css'))
    .pipe(dest('dist/wp/assets/css'))
}

function tsTask(){
    return src('src/ts/**/*.ts')
    .pipe(ts({
    }))
    .pipe(dest('dist/html/assets/js'))
    .pipe(dest('dist/wp/assets/js'))

}

// function js() {
//     return src('client/javascript/*.js', { sourcemaps: true })
//         .pipe(concat('app.min.js'))
//         .pipe(dest('build/js', { sourcemaps: true }))
// }
// function tsTask(_config) {
//     // typescriptトランスコンパイル設定ファイルの読み込み
//     const tsconfig = ts.createProject('./tsconfig.json');
//     return ()=> {
//         // tsファイルをes5の内容へ変換、指定先へ変換結果を出力
//         return gulp.src("typescriptファイルパスの指定")
//         .pipe(tsconfig()).js 
//         .pipe(gulp.dest("出力先パスの設定"));
//     };
// }
// gulp.task('ts', tsTask());

function watchTask(){
    watch(['src/pug/**/*.pug', 'src/scss/**/*.scss', 'src/ts/**/*.ts'],
        parallel(pugHtmlTask, pugWpTask, scssTask, tsTask)
    )
}

// exports.js = js;
// exports.scss = scss;
// exports.html = html;
// exports.default = parallel(html, css, js);
exports.default = series(
    parallel(pugHtmlTask, pugWpTask, scssTask, tsTask),
    watchTask
);